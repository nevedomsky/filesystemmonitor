// Copyright 2020 @nevack. All rights reserved.

#include "filesystemmonitor/notifier_builder.h"

namespace filesystemmonitor {

NotifierBuilder::NotifierBuilder() : mInotify(std::make_shared<Inotify>()) {}

NotifierBuilder BuildNotifier() { return {}; }

NotifierBuilder& NotifierBuilder::watchPathRecursively(
    boost::filesystem::path path) {
  mInotify->watchDirectoryRecursively(path);
  return *this;
}

NotifierBuilder& NotifierBuilder::watchFile(boost::filesystem::path file) {
  mInotify->watchFile(file);
  return *this;
}

NotifierBuilder& NotifierBuilder::unwatchFile(boost::filesystem::path file) {
  mInotify->unwatchFile(file);
  return *this;
}

NotifierBuilder& NotifierBuilder::ignoreFileOnce(boost::filesystem::path file) {
  mInotify->ignoreFileOnce(file.string());
  return *this;
}

NotifierBuilder& NotifierBuilder::ignoreFile(boost::filesystem::path file) {
  mInotify->ignoreFile(file.string());
  return *this;
}

NotifierBuilder& NotifierBuilder::ignoreFileName(std::string fileName) {
  mInotify->ignoreFileName(fileName);
  return *this;
}

NotifierBuilder& NotifierBuilder::onEvent(Event event,
                                          EventObserver eventObserver) {
  mInotify->setEventMask(mInotify->getEventMask() |
                         static_cast<std::uint32_t>(event));
  mEventObserver[event] = eventObserver;
  return *this;
}

NotifierBuilder& NotifierBuilder::onEvents(std::vector<Event> events,
                                           EventObserver eventObserver) {
  for (auto event : events) {
    mInotify->setEventMask(mInotify->getEventMask() |
                           static_cast<std::uint32_t>(event));
    mEventObserver[event] = eventObserver;
  }

  return *this;
}

NotifierBuilder& NotifierBuilder::onUnexpectedEvent(
    EventObserver eventObserver) {
  mUnexpectedEventObserver = eventObserver;
  return *this;
}

NotifierBuilder& NotifierBuilder::setEventTimeout(
    std::chrono::milliseconds timeout, EventObserver eventObserver) {
  auto onEventTimeout = [eventObserver](FileSystemEvent fileSystemEvent) {
    Notification notification{static_cast<Event>(fileSystemEvent.mask),
                              fileSystemEvent.path, fileSystemEvent.eventTime};
    eventObserver(notification);
  };

  mInotify->setEventTimeout(timeout, onEventTimeout);
  return *this;
}

void NotifierBuilder::runOnce() {
  auto fileSystemEvent = mInotify->getNextEvent();
  if (!fileSystemEvent) {
    return;
  }

  Event currentEvent = static_cast<Event>(fileSystemEvent->mask);

  Notification notification{currentEvent, fileSystemEvent->path,
                            fileSystemEvent->eventTime};

  for (auto& [event, eventObserver] : mEventObserver) {
    if (event == Event::all) {
      eventObserver(notification);
      return;
    }

    if (event == currentEvent) {
      eventObserver(notification);
      return;
    }
  }

  if (mUnexpectedEventObserver) {
    mUnexpectedEventObserver(notification);
  }
}

void NotifierBuilder::run() {
  while (true) {
    if (mInotify->hasStopped()) {
      break;
    }

    runOnce();
  }
}

void NotifierBuilder::stop() { mInotify->stop(); }

}  // namespace filesystemmonitor
