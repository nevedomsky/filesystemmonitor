// Copyright 2020 @nevack. All rights reserved.

#include <boost/filesystem.hpp>
#include <chrono>
#include <iostream>
#include <thread>

#include "filesystemmonitor/notifier_builder.h"

using namespace filesystemmonitor;

int main(int argc, char** argv) {
  if (argc <= 1) {
    std::cout << "Usage: ./inotify_example /path/to/dir" << std::endl;
    exit(0);
  }

  // Parse the directory to watch
  boost::filesystem::path path(argv[1]);

  // Set the event handler which will be used to process particular events
  auto handleNotification = [&](Notification notification) {
    std::cout << "Event " << notification.event << " on " << notification.path
              << " at " << notification.time.time_since_epoch().count()
              << " was triggered." << std::endl;
  };

  // Set the a separate unexpected event handler for all other events. An
  // exception is thrown by default.
  auto handleUnexpectedNotification = [](Notification notification) {
    std::cout << "Event " << notification.event << " on " << notification.path
              << " at " << notification.time.time_since_epoch().count()
              << " was triggered, but was not expected" << std::endl;
  };

  // Set the events to be notified for
  auto events = {
      Event::open | Event::is_dir,  // some events occur in combinations
      Event::access,
      Event::create,
      Event::modify,
      Event::remove,
      Event::move};

  // The notifier is configured to watch the parsed path for the defined events.
  // Particular files or paths can be ignored(once).
  auto notifier = BuildNotifier()
                      .watchPathRecursively(path)
                      .onEvents(events, handleNotification)
                      .onUnexpectedEvent(handleUnexpectedNotification);

  std::cout << "Configured" << std::endl;

  // The event loop is started in a separate thread context.
  std::thread thread([&]() { notifier.run(); });
  thread.join();
  return 0;
}
