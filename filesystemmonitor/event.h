// Copyright 2020 @nevack. All rights reserved.

#ifndef __EVENT_H__
#define __EVENT_H__

#include <sys/inotify.h>

#include <cstdint>
#include <iostream>

namespace filesystemmonitor {

/**
 * Inotify event types
 *
 * IN_ACCESS         File was accessed (read).
 * IN_ATTRIB         Metadata changed—for example, permissions,
 *                   timestamps, extended attributes, link count, UID, or GID.
 * IN_CLOSE_WRITE    File opened for writing was closed.
 * IN_CLOSE_NOWRITE  File not opened for writing was closed.
 * IN_CREATE         File/directory created in watched directory.
 * IN_DELETE         File/directory deleted from watched directory.
 * IN_DELETE_SELF    Watched file/directory was itself deleted.
 * IN_MODIFY         File was modified.
 * IN_MOVE_SELF      Watched file/directory was itself moved.
 * IN_MOVED_FROM     Generated for the directory containing the old
 *                   filename when a file is renamed.
 * IN_MOVED_TO       Generated for the directory containing the new
 *                   filename when a file is renamed.
 * IN_OPEN           File was opened.
 * IN_ALL_EVENTS     macro is defined as a bit mask of all of the above
 *                   events
 * IN_MOVE           IN_MOVED_FROM | IN_MOVED_TO
 * IN_CLOSE          IN_CLOSE_WRITE | IN_CLOSE_NOWRITE
 */

enum class Event : std::uint32_t {
  access = IN_ACCESS,
  attrib = IN_ATTRIB,
  close_write = IN_CLOSE_WRITE,
  close_nowrite = IN_CLOSE_NOWRITE,
  close = IN_CLOSE,
  create = IN_CREATE,
  remove = IN_DELETE,
  remove_self = IN_DELETE_SELF,
  modify = IN_MODIFY,
  move_self = IN_MOVE_SELF,
  moved_from = IN_MOVED_FROM,
  moved_to = IN_MOVED_TO,
  move = IN_MOVE,
  open = IN_OPEN,
  is_dir = IN_ISDIR,
  unmount = IN_UNMOUNT,
  q_overflow = IN_Q_OVERFLOW,
  ignored = IN_IGNORED,
  oneshot = IN_ONESHOT,
  all = IN_ALL_EVENTS
};

constexpr Event operator&(Event lhs, Event rhs) {
  return static_cast<Event>(
      static_cast<std::underlying_type<Event>::type>(lhs) &
      static_cast<std::underlying_type<Event>::type>(rhs));
}

constexpr Event operator|(Event lhs, Event rhs) {
  return static_cast<Event>(
      static_cast<std::underlying_type<Event>::type>(lhs) |
      static_cast<std::underlying_type<Event>::type>(rhs));
}

std::ostream& operator<<(std::ostream& stream, const Event& event);
bool containsEvent(const Event& allEvents, const Event& event);

}  // namespace filesystemmonitor

#endif  // __EVENT_H__
