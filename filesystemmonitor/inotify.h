// Copyright 2020 @nevack. All rights reserved.

#ifndef __INOTIFY_H__
#define __INOTIFY_H__

#include <assert.h>
#include <errno.h>
#include <sys/epoll.h>
#include <sys/inotify.h>
#include <time.h>

#include <atomic>
#include <boost/bimap.hpp>
#include <boost/filesystem.hpp>
#include <chrono>
#include <exception>
#include <map>
#include <memory>
#include <optional>
#include <queue>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include "filesystemmonitor/file_system_event.h"

#define MAX_EVENTS 4096
/**
 * MAX_EPOLL_EVENTS is set to 1 since there exists
 * only one eventbuffer. The value can be increased
 * when readEventsIntoBuffer can handle multiple
 * epoll events.
 */
#define MAX_EPOLL_EVENTS 1
#define EVENT_SIZE (sizeof(inotify_event))

namespace filesystemmonitor {

class Inotify {
 public:
  Inotify();
  ~Inotify();
  void watchDirectoryRecursively(boost::filesystem::path path);
  void watchFile(boost::filesystem::path file);
  void unwatchFile(boost::filesystem::path file);
  void ignoreFileOnce(boost::filesystem::path file);
  void ignoreFile(boost::filesystem::path file);
  void ignoreFileName(std::string fileName);
  void setEventMask(uint32_t eventMask);
  uint32_t getEventMask();
  void setEventTimeout(std::chrono::milliseconds eventTimeout,
                       std::function<void(FileSystemEvent)> onEventTimeout);
  std::optional<FileSystemEvent> getNextEvent();
  void stop();
  bool hasStopped();

 private:
  boost::filesystem::path wdToPath(int wd);
  bool isIgnored(std::string file);
  bool isOnTimeout(const std::chrono::steady_clock::time_point& eventTime);
  void removeWatch(int wd);
  ssize_t readEventsIntoBuffer(std::vector<uint8_t>& eventBuffer);
  void readEventsFromBuffer(uint8_t* buffer, int length,
                            std::vector<FileSystemEvent>& events);
  void filterEvents(std::vector<FileSystemEvent>& events,
                    std::queue<FileSystemEvent>& eventQueue);
  void sendStopSignal();

 private:
  int mError;
  std::chrono::milliseconds mEventTimeout;
  std::chrono::steady_clock::time_point mLastEventTime;
  uint32_t mEventMask;
  uint32_t mThreadSleep;
  std::vector<std::string> mIgnoredDirectories;
  std::vector<std::string> mOnceIgnoredDirectories;
  std::queue<FileSystemEvent> mEventQueue;
  boost::bimap<int, boost::filesystem::path> mDirectorieMap;
  int mInotifyFd;
  std::atomic<bool> mStopped;
  int mEpollFd;
  epoll_event mInotifyEpollEvent;
  epoll_event mStopPipeEpollEvent;
  epoll_event mEpollEvents[MAX_EPOLL_EVENTS];

  std::function<void(FileSystemEvent)> mOnEventTimeout;
  std::vector<uint8_t> mEventBuffer;

  int mStopPipeFd[2];
  const int mPipeReadIdx;
  const int mPipeWriteIdx;
};

}  // namespace filesystemmonitor

#endif  // __INOTIFY_H__
