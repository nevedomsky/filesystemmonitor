// Copyright 2020 @nevack. All rights reserved.

#ifndef __NOTIFICATION_H__
#define __NOTIFICATION_H__

#include <boost/filesystem.hpp>
#include <chrono>

#include "filesystemmonitor/event.h"

namespace filesystemmonitor {

class Notification {
 public:
  Notification(const Event& event, const boost::filesystem::path& path,
               std::chrono::steady_clock::time_point time);

 public:
  const Event event;
  const boost::filesystem::path path;
  const std::chrono::steady_clock::time_point time;
};
}  // namespace filesystemmonitor

#endif  // __NOTIFICATION_H__
