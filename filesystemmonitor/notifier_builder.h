// Copyright 2020 @nevack. All rights reserved.

#ifndef __NOTIFIER_BUILDER__
#define __NOTIFIER_BUILDER__

#include <boost/filesystem.hpp>
#include <memory>
#include <string>

#include "filesystemmonitor/inotify.h"
#include "filesystemmonitor/notification.h"

namespace filesystemmonitor {

using EventObserver = std::function<void(Notification)>;

class NotifierBuilder {
 public:
  NotifierBuilder();

  void run();
  void runOnce();
  void stop();
  NotifierBuilder& watchPathRecursively(boost::filesystem::path path);
  NotifierBuilder& watchFile(boost::filesystem::path file);
  NotifierBuilder& unwatchFile(boost::filesystem::path file);
  NotifierBuilder& ignoreFileOnce(boost::filesystem::path file);
  NotifierBuilder& ignoreFile(boost::filesystem::path file);
  NotifierBuilder& ignoreFileName(std::string fileName);
  NotifierBuilder& onEvent(Event event, EventObserver);
  NotifierBuilder& onEvents(std::vector<Event> event, EventObserver);
  NotifierBuilder& onUnexpectedEvent(EventObserver);
  NotifierBuilder& setEventTimeout(std::chrono::milliseconds timeout,
                       EventObserver eventObserver);

 private:
  std::shared_ptr<Inotify> mInotify;
  std::map<Event, EventObserver> mEventObserver;
  EventObserver mUnexpectedEventObserver;
};

NotifierBuilder BuildNotifier();
}  // namespace filesystemmonitor

#endif  // __NOTIFIER_BUILDER__
