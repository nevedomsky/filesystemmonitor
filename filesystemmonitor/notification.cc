// Copyright 2020 @nevack. All rights reserved.

#include "filesystemmonitor/notification.h"

namespace filesystemmonitor {

Notification::Notification(const Event& event,
                           const boost::filesystem::path& path,
                           std::chrono::steady_clock::time_point time)
    : event(event), path(path), time(time) {}

}  // namespace filesystemmonitor