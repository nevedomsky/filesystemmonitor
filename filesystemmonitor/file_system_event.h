// Copyright 2020 @nevack. All rights reserved.

#ifndef __FILE_SYSTEM_EVENT_H__
#define __FILE_SYSTEM_EVENT_H__

#include <boost/filesystem.hpp>
#include <chrono>
#include <string>

namespace filesystemmonitor {

class FileSystemEvent {
 public:
  FileSystemEvent(int wd, uint32_t mask, const boost::filesystem::path& path,
                  const std::chrono::steady_clock::time_point& eventTime);

  ~FileSystemEvent();

 public:
  int wd;
  uint32_t mask;
  boost::filesystem::path path;
  std::chrono::steady_clock::time_point eventTime;
};

}  // namespace filesystemmonitor

#endif  // __FILE_SYSTEM_EVENT_H__
